package connecttomysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author alex
 */
public class ConnectToMySQL {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        insert("Alice", "Test test");
        insert("Boby", "Nice nice");
        insert("Bobo", "buhu");
        
        select("Bob");
        select("s' OR 1=1; -- ");
        
    }
    
    public static void select(String name){
        try {
            Connection conn = DBConnector.getConnection();
            //name = name.replaceAll("'", "");// ' " Let's. Hello "Duck"
            
            String sql = "SELECT * FROM person WHERE name = ?;";

            System.out.println(sql);
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, name);
            pstmt.executeQuery();
            
            ResultSet rs = pstmt.getResultSet();
            while (rs.next()) {
                int id = rs.getInt("idPerson");
                String n = rs.getString("Name");
                String address = rs.getString("Address");
                System.out.println("id = " + id + " name = " + n + " address = " + address);
            }

        } catch (SQLException ex) {
            Logger.getLogger(ConnectToMySQL.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("------------End-----------");
    }
    
    
    public static void insert(String name, String address){
        try {
            Connection conn = DBConnector.getConnection();
            //INSERT INTO `firstdb`.`person` (`idPerson`, `Name`, `Address`) VALUES (NULL, 'Bob', 'test');
            String sql = "INSERT INTO person (Name, Address) VALUES (?, ?);";

            System.out.println(sql);
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, name);
            pstmt.setString(2, address);
            pstmt.execute();
        } catch (SQLException ex) {
            Logger.getLogger(ConnectToMySQL.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("------------End-----------");
    }

}
